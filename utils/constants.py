from enum import Enum


class Namespace(Enum):
    WALLET = "Wallet"
