from django.utils.translation import gettext_lazy as _
from rest_framework import status
from rest_framework.exceptions import APIException


class WalletNotFound(APIException):
    status_code = status.HTTP_404_NOT_FOUND
    default_detail = _("Wallet not found.")
    default_code = "wallet_not_found"


class InvalidFunctionType(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = _("Invalid function type")
    default_code = "invalid_function_type"
