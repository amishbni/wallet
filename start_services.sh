#!/bin/bash

case "$SERVICE" in
   "web")
      python manage.py makemigrations
      python manage.py migrate
      python manage.py collectstatic --noinput

      gunicorn wallet.wsgi:application --bind 0.0.0.0:8000
    ;;
   "celery_worker")
      celery -A wallet worker -Q celery -n main_worker -l INFO --concurrency=30
   ;;
esac