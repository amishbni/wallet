import dill
import json

from celery import current_app
from django.conf import settings
from django.utils import timezone as django_timezone
from unique_names_generator import get_random_name
from zoneinfo import ZoneInfo

from scheduler.models import ScheduledTask


def schedule_task(
        function,
        scheduled_time: str,
        timezone: str = None,
        args: tuple = None,
        kwargs: dict = None,
) -> str:
    serialized_args = json.dumps(args or [])
    serialized_kwargs = json.dumps(kwargs or {})
    timezone = timezone or settings.CELERY_TIMEZONE
    serialized_function = dill.dumps(function)

    scheduled_time = django_timezone.make_aware(
        django_timezone.datetime.strptime(scheduled_time, "%Y-%m-%d %H:%M:%S"),
        timezone=ZoneInfo(timezone),
    )

    name = get_random_name()
    ScheduledTask.objects.create(
        name=name,
        scheduled_time=scheduled_time,
        function=serialized_function,
        args=serialized_args,
        kwargs=serialized_kwargs,
    )

    current_app.send_task(
        "scheduler.tasks.execute_function",
        args=[serialized_function, *args],
        kwargs={"kwargs": serialized_kwargs},
        eta=scheduled_time,
    )
    return name
