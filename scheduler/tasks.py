import dill

from celery import shared_task


@shared_task
def execute_function(function, *args, **kwargs):
    function = dill.loads(function)
    return function(*args, **kwargs)
