from django.db import models
from picklefield.fields import PickledObjectField

from utils.models import BaseModel


class ScheduledTask(BaseModel):
    name = models.CharField(max_length=200)
    scheduled_time = models.DateTimeField()
    function = PickledObjectField()
    args = models.JSONField(null=True, blank=True)
    kwargs = models.JSONField(null=True, blank=True)

    def __str__(self):
        return self.name
