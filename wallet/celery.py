import os

import django
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'wallet.settings')
django.setup()

app = Celery("wallet")
app.config_from_object("django.conf:settings", namespace="CELERY")
app.autodiscover_tasks()
