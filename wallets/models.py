import uuid

from django.db import models, transaction
from django.db.models import F

from utils.models import BaseModel


class Transaction(BaseModel):
    class Type(models.TextChoices):
        DEPOSIT = "deposit", "Deposit"
        WITHDRAWAL = "withdrawal", "Withdrawal"

    wallet = models.ForeignKey("Wallet", on_delete=models.CASCADE, related_name="transactions")
    amount = models.BigIntegerField()
    type = models.CharField(max_length=10, choices=Type.choices)


class Wallet(BaseModel):
    uuid = models.UUIDField(default=uuid.uuid4, unique=True)
    balance = models.PositiveBigIntegerField(default=0)

    @transaction.atomic
    def deposit(self, amount: int, *args, **kwargs):
        if amount <= 0:
            raise ValueError("Deposit amount must be positive")

        Transaction.objects.create(
            wallet=self,
            amount=amount,
            type=Transaction.Type.DEPOSIT,
        )
        self.balance = F("balance") + amount
        self.save()

    @transaction.atomic
    def withdraw(self, amount: int, *args, **kwargs):
        if amount <= 0:
            raise ValueError("Withdraw amount must be positive")
        if amount > self.balance:
            raise ValueError("Insufficient balance")

        Transaction.objects.create(
            wallet=self,
            amount=amount,
            type=Transaction.Type.WITHDRAWAL,
        )
        self.balance = F("balance") - amount
        self.save()
