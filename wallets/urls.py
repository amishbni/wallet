from django.urls import path

from wallets.views import (
    CreateDepositView,
    ScheduleWithdrawView,
    CreateWalletView,
    RetrieveWalletView,
)

app_name = "wallets"
urlpatterns = [
    path("", CreateWalletView.as_view(), name="create-wallet"),
    path("<uuid>/", RetrieveWalletView.as_view(), name="retrieve-wallet"),
    path("<uuid>/deposit", CreateDepositView.as_view(), name="deposit"),
    path("<uuid>/withdraw", ScheduleWithdrawView.as_view(), name="withdraw"),
]
