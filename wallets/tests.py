from django.test import TestCase
from django.db import transaction
from django.db.models import QuerySet
from django.db.utils import IntegrityError

from wallets.models import Wallet, Transaction


class WalletTestCases(TestCase):
    def setUp(self):
        super().setUp()
        self.initial_balance = 1_000
        self.wallet: Wallet = Wallet.objects.create(balance=self.initial_balance)

    def test_deposit_positive(self):
        initial_balance: int = self.wallet.balance
        deposit_amount: int = 1_000

        self.wallet.deposit(amount=deposit_amount)
        self.wallet.refresh_from_db()

        self.assertEqual(self.wallet.balance, initial_balance + deposit_amount)

        transactions: QuerySet[Transaction] = self.wallet.transactions.all()
        self.assertEqual(transactions.count(), 1)

        transaction: Transaction = transactions.first()
        self.assertEqual(transaction.wallet, self.wallet)
        self.assertEqual(transaction.amount, deposit_amount)
        self.assertEqual(transaction.type, Transaction.Type.DEPOSIT)

    def test_deposit_zero_or_negative(self):
        with self.assertRaises(ValueError):
            self.wallet.deposit(amount=0)

        with self.assertRaises(ValueError):
            self.wallet.deposit(amount=-1_000)

        self.wallet.refresh_from_db()
        self.assertEqual(self.wallet.balance, self.initial_balance)
        self.assertEqual(self.wallet.transactions.count(), 0)

    def test_withdrawal_positive(self):
        initial_balance: int = self.wallet.balance
        withdrawal_amount: int = 1_000

        self.wallet.withdraw(amount=withdrawal_amount)
        self.wallet.refresh_from_db()
        self.assertEqual(self.wallet.balance, initial_balance - withdrawal_amount)

        transactions: QuerySet[Transaction] = self.wallet.transactions.all()
        self.assertEqual(transactions.count(), 1)

        transaction: Transaction = transactions.first()
        self.assertEqual(transaction.wallet, self.wallet)
        self.assertEqual(transaction.amount, withdrawal_amount)
        self.assertEqual(transaction.type, Transaction.Type.WITHDRAWAL)

    def test_withdrawal_zero_or_negative(self):
        with self.assertRaises(ValueError):
            self.wallet.withdraw(amount=0)

        with self.assertRaises(ValueError):
            self.wallet.withdraw(amount=-1_000)

        self.wallet.refresh_from_db()
        self.assertEqual(self.wallet.balance, self.initial_balance)
        self.assertEqual(self.wallet.transactions.count(), 0)

    def test_withdrawal_insufficient_balance(self):
        with self.assertRaises(ValueError):
            self.wallet.withdraw(amount=self.initial_balance + 1)

        self.wallet.refresh_from_db()
        self.assertEqual(self.wallet.balance, self.initial_balance)
        self.assertEqual(self.wallet.transactions.count(), 0)

    def test_atomicity_of_deposit(self):
        with self.assertRaises(IntegrityError):
            with transaction.atomic():
                self.wallet.deposit(amount=1_000)
                raise IntegrityError("Unexpected error")

        self.wallet.refresh_from_db()
        self.assertEqual(self.wallet.balance, self.initial_balance)
        self.assertEqual(self.wallet.transactions.count(), 0)

    def test_atomicity_of_withdrawal(self):
        with self.assertRaises(IntegrityError):
            with transaction.atomic():
                self.wallet.withdraw(amount=1_000)
                raise IntegrityError("Unexpected error")

        self.wallet.refresh_from_db()
        self.assertEqual(self.wallet.balance, self.initial_balance)
        self.assertEqual(self.wallet.transactions.count(), 0)
