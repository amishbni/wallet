import json
from rest_framework import status

from rest_framework.generics import CreateAPIView, RetrieveAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from scheduler.services import schedule_task

from utils.constants import Namespace
from utils.exceptions import WalletNotFound
from utils.models import ExtendedSchema

from wallets.models import Wallet
from wallets.serializers import WalletSerializer, DepositSerializer, WithdrawalSerializer


class CreateWalletView(ExtendedSchema, CreateAPIView):
    schema_tags = [Namespace.WALLET.value]
    serializer_class = WalletSerializer


class RetrieveWalletView(ExtendedSchema, RetrieveAPIView):
    schema_tags = [Namespace.WALLET.value]
    serializer_class = WalletSerializer
    queryset = Wallet.objects.all()
    lookup_field = "uuid"


class CreateDepositView(ExtendedSchema, APIView):
    schema_tags = [Namespace.WALLET.value]
    serializer_class = DepositSerializer

    def post(self, request, *args, **kwargs):
        try:
            serializer = self.serializer_class(data=request.data, context={"uuid": kwargs.get("uuid")})

            if serializer.is_valid():
                amount = serializer.validated_data['amount']
                uuid = serializer.context.get("uuid")
                wallet = Wallet.objects.get(uuid=uuid)
                wallet.deposit(amount=amount)
                return Response(serializer.data, status=status.HTTP_200_OK)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except Wallet.DoesNotExist:
            raise WalletNotFound()


class ScheduleWithdrawView(ExtendedSchema, APIView):
    schema_tags = [Namespace.WALLET.value]
    serializer_class = WithdrawalSerializer

    def post(self, request, *args, **kwargs):
        try:
            serializer = self.serializer_class(data=request.data, context={"uuid": kwargs.get("uuid")})

            if serializer.is_valid():
                amount = serializer.validated_data['amount']
                scheduled_time = serializer.validated_data['scheduled_time']
                timezone = serializer.validated_data['timezone']
                uuid = serializer.context.get("uuid")
                wallet = Wallet.objects.get(uuid=uuid)
                schedule_task(
                    function=wallet.withdraw,
                    scheduled_time=scheduled_time,
                    timezone=timezone,
                    args=(amount,)
                )
                return Response(serializer.data, status=status.HTTP_200_OK)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except Wallet.DoesNotExist:
            raise WalletNotFound()
