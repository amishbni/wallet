from django.conf import settings
from django.utils import timezone as django_timezone
from rest_framework import serializers

from wallets.models import Wallet, Transaction


class WalletSerializer(serializers.ModelSerializer):
    class Meta:
        model = Wallet
        fields = ("uuid", "balance")
        read_only_fields = ("uuid", "balance")


class DepositSerializer(serializers.ModelSerializer):
    uuid = serializers.SerializerMethodField()
    amount = serializers.IntegerField()

    class Meta:
        model = Transaction
        fields = (
            "uuid",
            "amount",
        )

    def get_uuid(self, obj) -> str:
        return self.context.get("uuid")


class WithdrawalSerializer(serializers.ModelSerializer):
    uuid = serializers.SerializerMethodField()
    amount = serializers.IntegerField(write_only=True)
    scheduled_time = serializers.CharField(
        default=django_timezone.localtime(
            django_timezone.now() + django_timezone.timedelta(minutes=1)
        ).strftime("%Y-%m-%d %H:%M:%S")
    )
    timezone = serializers.CharField(default=settings.TIME_ZONE)

    class Meta:
        model = Transaction
        fields = (
            "uuid",
            "amount",
            "scheduled_time",
            "timezone",
        )

    def get_uuid(self, obj) -> str:
        return self.context.get("uuid")
